from controller_interface import ControllerInterface
import random
import math
import time
import copy
import csv

class Controller(ControllerInterface):

    def __init__(self, simulation, gs=None):
        self.gs = gs
        self.simulation = simulation
        self.episode = 0
        self.sensors = simulation.drill_1.sensors
        self.controller_2 = None
        self.controller_2_w = None
        self.prev_player_pos = ( 200, 350 )
        self.prev_asteroid_pos = ( 0, 0 )

        self.number_of_features = 8
        self.algorithm = "local_beam"
        self.timeout = 2400

    def take_action(self, weights: tuple) -> int:

        features = Controller.compute_features( self, self.sensors )

        turn_right = weights[0]
        i = 1
        for j in range(len(features)):
            turn_right += (weights[i] * features[j])
            i += 1

        turn_left = weights[i]
        i += 1
        for j in range(len(features)):
            turn_left += (weights[i] * features[j])
            i += 1

        accelerate = weights[i]
        i += 1
        for j in range(len(features)):
            accelerate += (weights[i] * features[j])
            i += 1

        taser = weights[i]
        i += 1
        for j in range(len(features)):
            taser += (weights[i] * features[j])
            i += 1

        do_nothing = weights[i]
        i += 1
        for j in range(len(features)):
            do_nothing += (weights[i] * features[j])
            i += 1

        if accelerate > turn_right and accelerate > turn_left and accelerate > taser and accelerate > do_nothing:
            return self.ACCELERATE
        elif turn_right >= turn_left and turn_right >= accelerate and turn_right >= taser and turn_right >= do_nothing:
            return self.RIGHT
        elif turn_left > turn_right and turn_left >= accelerate and turn_left >= taser and turn_left >= do_nothing:
            return self.LEFT
        elif taser >= turn_right and taser >= turn_left and taser >= accelerate and taser >= do_nothing:
            return self.DISCHARGE
        else:
            return self.NOTHING

    def compute_features(self, sensors: dict) -> tuple:

        player_pos = sensors['drill_position']
        drill_edge_pos = sensors['drill_edge_position']
        asteroid_pos = sensors['asteroid_position']
        enemy_pos = sensors['enemy_1_drill_position']
        mothership_pos = sensors['drill_mothership_position']
        enemy_mothership_pos = sensors['enemy_1_drill_mothership_position']

        # implemented features
        enough_fuel = sensors['drill_gas']
        enough_fuel = ( enough_fuel / 100 ) - 1

        diff = 0
        x = abs(self.prev_player_pos[0] - self.prev_asteroid_pos[0])
        y = abs(self.prev_player_pos[1] - self.prev_asteroid_pos[1])
        previous_distance = math.hypot(x, y)
        x = abs(player_pos[0] - asteroid_pos[0])
        y = abs(player_pos[1] - asteroid_pos[1])
        current_distance = math.hypot(x, y)
        if (current_distance < previous_distance):
            diff = 1

        collecting_resources = sensors['drill_touching_asteroid']
        pointing_at_asteroid = sensors['align_asteroid']

        enemy_stealing = 0
        x = abs(enemy_pos[0] - mothership_pos[0])
        y = abs(enemy_pos[1] - mothership_pos[1])
        enemy_player_ms_dist = math.hypot(x, y)
        x = abs(enemy_pos[0] - enemy_mothership_pos[0])
        y = abs(enemy_pos[1] - enemy_mothership_pos[1])
        enemy_own_ms_dist = math.hypot(x, y)
        if ( sensors['enemy_1_drill_touching_mothership'] and enemy_own_ms_dist > enemy_player_ms_dist ):
            enemy_stealing = 1

        pointing_at_mothership = sensors['align_mothership']

        asteroid_enemy_resources_diff = sensors['asteroid_resources'] - sensors['enemy_1_drill_resources']
        asteroid_enemy_resources_diff = ( 2 * (asteroid_enemy_resources_diff + 150) / 400 ) - 1

        x = abs(player_pos[0] - asteroid_pos[0])
        y = abs(player_pos[1] - asteroid_pos[1])
        player_asteroid_dist = math.hypot(x, y)
        x = abs(player_pos[0] - enemy_mothership_pos[0])
        y = abs(player_pos[1] - enemy_mothership_pos[1])
        player_enemy_ms_dist = math.hypot(x, y)
        player_distances_diff = player_asteroid_dist - player_enemy_ms_dist
        player_distances_diff = ( 2 * ( player_distances_diff + 1220 ) / 2440 ) - 1

        player_stealing = 0
        x = abs(player_pos[0] - enemy_mothership_pos[0])
        y = abs(player_pos[1] - enemy_mothership_pos[1])
        player_enemy_ms_dist = math.hypot(x, y)
        x = abs(player_pos[0] - mothership_pos[0])
        y = abs(player_pos[1] - mothership_pos[1])
        player_own_ms_dist = math.hypot(x, y)
        if ( sensors['drill_touching_mothership'] and player_own_ms_dist > player_enemy_ms_dist ):
            enemy_stealing = 1

        drill_vector = (drill_edge_pos[0] - player_pos[0], drill_edge_pos[1] - player_pos[1])
        drill_vector_norm = math.hypot(drill_vector[0], drill_vector[1])
        asteroid_vector = (asteroid_pos[0] - player_pos[0], asteroid_pos[1] - player_pos[1])
        asteroid_vector_norm = math.hypot(asteroid_vector[0], asteroid_vector[1])
        cos_t = ( ( drill_vector[0] * asteroid_vector[0] ) + ( drill_vector[1] * asteroid_vector[1] ) ) / (drill_vector_norm * asteroid_vector_norm)
        cos_t = min(1, cos_t)
        cos_t = max(-1, cos_t)
        player_asteroid_angle = math.acos(cos_t)
        player_asteroid_angle = ( player_asteroid_angle / math.pi ) - 1

        player_moving_to_mothership = 0
        x = abs(self.prev_player_pos[0] - mothership_pos[0])
        y = abs(self.prev_player_pos[1] - mothership_pos[1])
        previous_ms_distance = math.hypot(x, y)
        x = abs(player_pos[0] - mothership_pos[0])
        y = abs(player_pos[0] - mothership_pos[0])
        current_ms_distance = math.hypot(x, y)
        if (current_ms_distance < previous_ms_distance):
            player_moving_to_mothership = 1

        taser_cooldown = sensors['drill_discharge_cooldown']
        taser_cooldown = ( 2 * taser_cooldown / 100 ) - 1

        self.prev_player_pos = player_pos
        self.prev_asteroid_pos = asteroid_pos

        # features: diff, collecting_resources, pointing_at_asteroid, enough_fuel, enemy_stealing, pointing_at_mothership, asteroid_enemy_resources_diff, player_distances_diff, player_stealing, player_asteroid_angle, player_moving_to_mothership, taser_cooldown

        if (self.number_of_features == 4):
            # features set 1 (4 features)
            return (diff, collecting_resources, pointing_at_asteroid, enough_fuel)
        elif (self.number_of_features == 8):
            # features set 2 (8 features)
            return (diff, collecting_resources, player_asteroid_angle, pointing_at_asteroid, enough_fuel, pointing_at_mothership, player_moving_to_mothership, enemy_stealing)
        elif (self.number_of_features == 12):
            # features set 3 (all 12 features)
            return (diff, collecting_resources, player_asteroid_angle, enough_fuel, player_moving_to_mothership, pointing_at_mothership, asteroid_enemy_resources_diff, player_distances_diff, player_stealing, enemy_stealing, taser_cooldown)
        elif (self.number_of_features == 6):
            # intermediate set
            return (diff, collecting_resources, player_asteroid_angle, enough_fuel, pointing_at_mothership, player_moving_to_mothership)

    def learn(self, weights: tuple):
        number_of_parameters = ( self.number_of_features + 1 ) * 5

        if ( self.algorithm == "local_beam" ):
            # below: local beam search
            # adjustable parameters
            k = 15

            # initial solutions
            solutions = []
            best_scores = []
            for i in range(k):
                new_solution = []
                for j in range( number_of_parameters ):
                    new_solution.append( random.uniform(-1, 1) )
                solutions.append( new_solution )
                best_scores.append( min( Controller.run_episode( self, new_solution ), Controller.run_episode( self, new_solution ) ) )
            """
            print( "Initial scores: " )
            for score in best_scores:
                print( "", "\t" + str(score), "" )
            """
            start = time.clock()
            end = start

            while ( end - start < self.timeout ):
                # generate neighbors
                neighbors = []
                for solution in solutions:
                    for i in range(k):
                        new_neighbor = solution[:]
                        for j in range( len(new_neighbor) ):
                            new_neighbor[j] += random.uniform(-1, 1)
                        neighbors.append( new_neighbor )

                # evaluate neighbors
                for neighbor in neighbors:
                    neighbor_score = min( Controller.run_episode( self, neighbor ), Controller.run_episode( self, neighbor ) )

                    # find min best score
                    min_best = 1000000000
                    min_index = 0
                    for i in range( len(best_scores) ):
                        if ( best_scores[i] < min_best ):
                            min_best = best_scores[i]
                            min_index = i

                    if ( neighbor_score >= min_best ):
                        best_scores[min_index] = neighbor_score
                        solutions[min_index] = neighbor[:]

                #print( "Scores: " )
                avg = 0
                max_score = 0
                for score in best_scores:
                    avg += score
                    if ( score >= max_score ):
                        max_score = score
                    #print( "", "\t" + str(score), "" )
                avg /= k
                """
                print( "Average: " + str( avg ) )
                print( "Max: " + str( max_score ) )
                print( "Episodes ran so far: " + str( self.episode ) )
                """
                filenamecsv = 'beam_'+str(self.number_of_features)+'-'+str(k)+'.csv'
                with open(filenamecsv, 'a', newline='') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=['Avg', 'Max', 'Episodes ran'])
                    writer.writerow({'Episodes ran': self.episode, 'Max': max_score, 'Avg': avg})
                csvfile.close()

                end = time.clock()

            # finding best solution
            max_score = 0
            best_solution = []
            for i in range( len( best_scores ) ):
                if ( best_scores[i] > max_score ):
                    max_score = best_scores[i]
                    best_solution = solutions[i][:]
            """
            print( "Max score: " + str(max_score) )
            print( "Iterations: " + str(iterations_local_beam) )
            print( "Episodes ran: " + str(self.episode) )
            """
            filename = "weights_beam_" + str( self.number_of_features ) + ".txt"
            f = open( filename, "w+" )
            for weight in best_solution:
                f.write( str(weight) + "\n" )
            f.close()

            return best_solution

        elif ( self.algorithm == "genetic_algorithm" ):
            population = 30
            prob_mut = 0.003
            individuals = []
            fitness = []

            # initial population
            for i in range( population ):
                chromossomes = []
                for j in range( number_of_parameters ):
                    chromossomes.append( random.uniform( -1, 1 ) )
                individuals.append( chromossomes )
                fitness.append( min( Controller.run_episode( self, chromossomes ), Controller.run_episode( self, chromossomes ) ) )

            # next generations
            new_individuals = []
            new_fitness = []

            start = time.clock()
            end = start
            while( end - start <= self.timeout ):
                # steady state selection and uniform crossover
                for k in range( round( population/2 ) ):
                    # roulette selection of parents
                    s = 0
                    parents = [0, 0]
                    for i in range( len( fitness ) ):
                        s += fitness[i]

                    r = random.uniform( 0, s )
                    t = 0
                    for i in range( len( individuals ) ):
                        t += fitness[i]
                        if ( t >= r ):
                            parents[0] = i
                            break
                    r = random.uniform( 0, s )
                    t = 0
                    for i in range( len( individuals ) ):
                        t += fitness[i]
                        if ( t >= r and i != parents[0] ):
                            parents[1] = i
                            break

                    # define heritage probability
                    total_fitness = fitness[parents[0]] + fitness[parents[1]]
                    heritage_prob = fitness[parents[0]] / max(total_fitness, 0.0000000001)

                    parent_1 = individuals[parents[0]]
                    parent_2 = individuals[parents[1]]

                    # generate children
                    child_1 = []
                    child_2 = []
                    for i in range( number_of_parameters ):
                        mask = random.uniform( 0, 1 )
                        if ( mask <= heritage_prob ):
                            gene_1 = parent_1[i]
                            gene_2 = parent_2[i]
                        else:
                            gene_1 = parent_2[i]
                            gene_2 = parent_1[i]
                        # chances of mutation in child 1 or 2
                        if ( random.uniform( 0, 1 ) < prob_mut ):
                            gene_1 = random.uniform( -1, 1 )
                        if ( random.uniform( 0, 1 ) < prob_mut ):
                            gene_2 = random.uniform( -1, 1 )

                        child_1.append( gene_1 )
                        child_2.append( gene_2 )

                    # selects 2 best individuals
                    child_1_fitness = min( Controller.run_episode( self, child_1 ), Controller.run_episode( self, child_1 ) )
                    child_2_fitness = min( Controller.run_episode( self, child_2 ), Controller.run_episode( self, child_2 ) )
                    parent_1_fitness = fitness[parents[0]]
                    parent_2_fitness = fitness[parents[1]]

                    if ( child_1_fitness >= parent_1_fitness and child_1_fitness >= parent_2_fitness and child_2_fitness >= parent_1_fitness and child_2_fitness >= parent_2_fitness ):
                        new_individuals.append(child_1)
                        new_fitness.append(child_1_fitness)
                        new_individuals.append(child_2)
                        new_fitness.append(child_2_fitness)
                    elif ( child_1_fitness >= child_2_fitness and child_1_fitness >= parent_2_fitness and parent_1_fitness >= child_2_fitness and parent_1_fitness >= parent_2_fitness ):
                        new_individuals.append(child_1)
                        new_fitness.append(child_1_fitness)
                        new_individuals.append(parent_1)
                        new_fitness.append(parent_1_fitness)
                    elif ( child_1_fitness >= child_2_fitness and child_1_fitness >= parent_1_fitness and parent_2_fitness >= child_2_fitness and parent_2_fitness >= parent_1_fitness ):
                        new_individuals.append(child_1)
                        new_fitness.append(child_1_fitness)
                        new_individuals.append(parent_2)
                        new_fitness.append(parent_2_fitness)
                    elif ( child_2_fitness >= child_1_fitness and child_2_fitness >= parent_2_fitness and parent_1_fitness >= child_1_fitness and parent_1_fitness >= parent_2_fitness ):
                        new_individuals.append(child_2)
                        new_fitness.append(child_2_fitness)
                        new_individuals.append(parent_1)
                        new_fitness.append(parent_1_fitness)
                    elif ( child_2_fitness >= child_1_fitness and child_2_fitness >= parent_1_fitness and parent_2_fitness >= child_1_fitness and parent_2_fitness >= parent_1_fitness ):
                        new_individuals.append(child_2)
                        new_fitness.append(child_2_fitness)
                        new_individuals.append(parent_2)
                        new_fitness.append(parent_2_fitness)
                    else:
                        new_individuals.append(parent_1)
                        new_fitness.append(parent_1_fitness)
                        new_individuals.append(parent_2)
                        new_fitness.append(parent_2_fitness)

                # find fittest so far
                max_fitness = 0
                index = 0
                avg = 0
                for i in range( len(new_fitness) ):
                    avg += new_fitness[i]
                    if ( new_fitness[i] >= max_fitness ):
                        max_fitness = new_fitness[i]
                        index = i
                avg /= population
                """
                print( "\nScore: " + str( max_fitness ) )
                print( "Average: " + str( avg ) )
                print( "Episodes so far: " + str( self.episode ) + "\n" )
                """
                filenamecsv = 'gen_'+str(self.number_of_features)+'-'+str(population)+'-'+str(prob_mut)+'.csv'
                with open(filenamecsv, 'a+', newline='') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=['Episodes ran','Max','Avg'])
                    writer.writerow({'Episodes ran': self.episode, 'Max': max_fitness, 'Avg': avg})
                csvfile.close()
                # updates population and fitness
                individuals = copy.deepcopy(new_individuals)
                fitness = copy.deepcopy(new_fitness)
                new_individuals.clear()
                new_fitness.clear()

                end = time.clock()

            # finds overall best individual
            max_fitness = 0
            index = 0
            i = 0
            for i in range( len(fitness) ):
                if ( fitness[i] >= max_fitness ):
                    max_fitness = fitness[i]
                    index = i

            print( "\nMax score: " + str( max_fitness ) )
            print( "Total episodes: " + str( self.episode ) )

            fittest = individuals[index][:]
            filename = "weights_genetic_" + str( self.number_of_features ) + ".txt"
            f = open( filename, "w+" )
            for weight in fittest:
                f.write(str(weight))
                f.write( "\n" )
            f.close()

            return fittest

        elif ( self.algorithm == "simulated_annealing" ):
            T = 100
            number_of_neighbors = 20
            perturbation = 0.4
            t = 0.999
            current_s = []
            current_s = copy.deepcopy(weights)
            current_score = min( Controller.run_episode( self, current_s ), Controller.run_episode( self, current_s ) )
            best_s = []
            best_s = copy.deepcopy(current_s)
            best_scr = current_score
            while ( T >= 1 ):
                scoresSA = 0
                for j in range(number_of_neighbors):
                    new_s = copy.deepcopy(current_s)
                    # new random state
                    for i in range( len( new_s ) ):
                        new_s[i] += random.uniform(-perturbation, perturbation)
                    new_score = Controller.run_episode( self, new_s )
                    scoresSA += new_score
                    delta = new_score - current_score
                    if ( delta >= 0 ):
                        current_s = copy.deepcopy(new_s)
                        current_score = new_score
                        if current_score > best_scr:
                            best_scr = current_score
                            best_s = copy.deepcopy(current_s)
                    else:
                        prob = random.uniform( 0, 1 )
                        exp = math.exp( delta/T )
                        # accepts worse states
                        if prob <= exp:
                            current_s = copy.deepcopy(new_s)
                            current_score = new_score
                filenamecsv = 'ann_'+str(self.number_of_features)+'-'+str(T)+'.csv'
                with open(filenamecsv, 'a+', newline='') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=['Episodes ran','Max','Curr'])
                    writer.writerow({'Episodes ran': self.episode, 'Max': best_scr, 'Curr': current_score})
                csvfile.close()
                x = int(self.episodes)
                T *= t**(int(x))

            filename = "weights_annealing--" + str( self.number_of_features ) + ".txt"
            f = open( filename, "w+" )
            for weight in best_s:
                f.write(str(weight))
                f.write( "\n" )
            f.close()

            return best_s

        else:
            print( "Invalid algorithm" )
